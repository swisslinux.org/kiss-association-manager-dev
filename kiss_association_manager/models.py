from django.db import models
from django.utils.translation import gettext as _

from datetime import date


YEAR_CODE = 'YEAR'
SEMESTER_CODE = 'SMSTR'
TRIMESTER_CODE = 'TRMST'
MONTH_CODE = 'MONTH'
PERIOD_LENGTH_CHOICES = (
    (YEAR_CODE, _('year')),
    # (SEMESTER_CODE, _('semester')),
    # (TRIMESTER_CODE, _('trimester')),
    # (MONTH_CODE, _('month')),
)


class Role(models.Model):
    """A role, for one or many members"""
    title = models.CharField(
        max_length=255,
        verbose_name=_('title'),
    )
    
    class Meta:
        verbose_name = _('role')
        verbose_name_plural = _('roles')

    def __str__(self):
        return '{}'.format(self.title)


class MemberType(models.Model):
    """The type of a member (Physical person, association, etc)"""
    title = models.CharField(
        max_length=255,
        verbose_name=_('title'),
    )
    membership_fee_amount = models.FloatField(
        verbose_name=_('membership fee amount'),
    )
    membership_fee_frequencies = models.CharField(
        max_length=5,
        verbose_name=_('membership fee frequencies'),
        choices=PERIOD_LENGTH_CHOICES,
        default=YEAR_CODE,
    )
    
    class Meta:
        verbose_name = _('member type')
        verbose_name_plural = _('member types')

    def __str__(self):
        return '{}'.format(self.title)


class Address(models.Model):
    """An address"""
    street = models.CharField(
        max_length=255,
        verbose_name=_('street'),
    )
    postal_code = models.CharField(
        max_length=255,
        verbose_name=_('postal code'),
    )
    city = models.CharField(
        max_length=255,
        verbose_name=_('city'),
    )
    state = models.CharField(
        max_length=255,
        verbose_name=_('state'),
    )
    country = models.CharField(
        max_length=255,
        verbose_name=_('country'),
    )
    
    class Meta:
        verbose_name = _('address')
        verbose_name_plural = _('addresses')

    def __str__(self):
        return '{}, {} {}'.format(
            self.street,
            self.postal_code,
            self.city,
        )


class Member(models.Model):
    """Member of your association"""
    REQUEST_CODE = 'REQ'
    MEMBER_CODE = 'MEM'
    QUIT_CODE = 'QUT'
    BANNED_CODE = 'BAN'
    STATE_CHOICES = (
        (REQUEST_CODE, _('Join request')),
        (MEMBER_CODE, _('Member')),
        (QUIT_CODE, _('Quit')),
        (BANNED_CODE, _('Banned'))
    )
    STATUS_PAYING_FEES = (
        MEMBER_CODE,
    )
    
    forname = models.CharField(
        max_length=255,
        verbose_name=_('forname'),
    )
    name = models.CharField(
        max_length=255,
        verbose_name=_('name'),
    )
    registration_date = models.DateField(
        verbose_name=_('registration date'),
        auto_now_add=True,
    )
    address = models.ForeignKey(
        Address,
        on_delete=models.CASCADE,
        verbose_name=_('address'),
    )
    email = models.EmailField(
        max_length=255,
        verbose_name=_('email'),
    )
    state = models.CharField(
        max_length=5,
        verbose_name=_('state'),
        choices=STATE_CHOICES,
        default=REQUEST_CODE,
    )
    roles = models.ManyToManyField(
        Role,
        verbose_name=_('roles'),
        blank=True,
#        null=True,
    )
    member_type = models.ForeignKey(
        MemberType,
        on_delete=models.CASCADE,
        verbose_name=_('member type'),
    )
    
    class Meta:
        verbose_name = _('member')
        verbose_name_plural = _('members')

    def __str__(self):
        return '{} {}'.format(
            self.forname,
            self.name,
        )


class MembershipFee(models.Model):
    """A membership fees to pay. Linked to a member on a specific period."""

    TO_PAY_CODE = 'topay'
    PARTIALLY_PAID_CODE = 'ppaid'
    PAID_CODE = 'paid'
    PAYMENT_STATUS_CHOICES = (
        (TO_PAY_CODE, _('to pay')),
        (PARTIALLY_PAID_CODE, _('partially paid')),
        (PAID_CODE, _('paid')),
    )
    
    title = models.CharField(
        max_length=255,
        verbose_name=_('title'),
    )
    member = models.ForeignKey(
        Member,
        on_delete=models.CASCADE,
        verbose_name=_('member'),
    )
    creation_date = models.DateField(
        verbose_name=_('creation date'),
        auto_now_add=True,
    )
    sending_date = models.DateField(
        verbose_name=_('sending date'),
        blank=True,
        null=True,
    )
    transfer_id = models.IntegerField(
        verbose_name=_('transfer id'),
        blank=True,
        null=True,
    )
    amount = models.FloatField(
        verbose_name=_('amount'),
    )
    from_date = models.DateField(
        verbose_name=_('from date'),
    )
    to_date = models.DateField(
        verbose_name=_('to date'),
    )
    period_length = models.CharField(
        max_length=5,
        verbose_name=_('period length'),
        choices=PERIOD_LENGTH_CHOICES,
        default=YEAR_CODE,
    )
    payment_status = models.CharField(
        max_length=5,
        verbose_name=_('payment status'),
        choices=PAYMENT_STATUS_CHOICES,
        default=TO_PAY_CODE,
    )
    notes = models.TextField(
        verbose_name=_('notes'),
        blank=True,
        null=True,
    )
    
    class Meta:
        verbose_name = _('membership fee')
        verbose_name_plural = _('membership fees')

    def __str__(self):
        return '{}'.format(self.title)

    def is_sent(self):
        "Return True if sending date is not null"
        return True if self.sending_date else False
        
    @classmethod
    def generate_missing_annuals(cls, year):
        "Generate all missing annuals membership fees for a specific year"
        
        members_with_a_membership_fee_pk = list(
            MembershipFee.objects.filter(
                period_length=YEAR_CODE,
                from_date__year=year,
                to_date__year=year,
            ).values_list('member__pk', flat=True)
        )

        members_needing_a_membership_fee = list(
            Member.objects.filter(
                member_type__membership_fee_frequencies=YEAR_CODE,
                state__in=Member.STATUS_PAYING_FEES,
            ).exclude(
                pk__in=members_with_a_membership_fee_pk,
            )
        )

        for member in members_needing_a_membership_fee:
            MembershipFee.objects.create(
                title=_('Membership fee of {} {} for year {}').format(
                    member.forname,
                    member.name,
                    year,
                ),
                member=member,
                creation_date=date.today(),
                amount=member.member_type.membership_fee_amount,
                from_date=date(year, 1, 1),
                to_date=date(year, 12, 31),
                period_length=YEAR_CODE,
                payment_status=cls.TO_PAY_CODE,
            )


class InternalRegulation(models.Model):
    """Internal regulation of the association"""

    version = models.CharField(
        max_length=255,
        verbose_name=_('version'),
    )
    text = models.TextField(
        verbose_name=_('text'),
    )
    is_applied = models.BooleanField(
        verbose_name=_('is applied'),
    )

    def save(self, check_unique_applied=True, *args, **kwargs):
        "Save the instance"
        if self.is_applied and check_unique_applied:
            try:
                already_applied = InternalRegulation.objects.filter(is_applied=True)
                for intern_regul in already_applied:
                    if intern_regul != self:
                        intern_regul.is_applied = False
                        intern_regul.save(check_unique_applied=False)
            except InternalRegulation.DoesNotExist:
                pass
        return super(InternalRegulation, self).save(*args, **kwargs)
    
    class Meta:
        verbose_name = _('internal regulation')
        verbose_name_plural = _('internal regulations')

    def __str__(self):
        return _('Version {}').format(self.version)

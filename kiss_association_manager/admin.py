from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import (
    Role,
    MemberType,
    Address,
    Member,
    MembershipFee,
    InternalRegulation,
)


@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    "How the Role model can be manage from admin"


@admin.register(MemberType)
class MemberTypeAdmin(admin.ModelAdmin):
    "How the MemberType model can be manage from admin"
    list_display = (
        'title',
        'membership_fee_amount',
        'membership_fee_frequencies',
    )
    list_filter = (
        'membership_fee_frequencies',
    )


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    "How the Address model can be manage from admin"


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    "How the Member model can be manage from admin"
    list_display = (
        'name',
        'forname',
        'registration_date',
        'state',
        'get_roles_as_string',
        'member_type',
    )
    list_filter = (
        'state',
        'member_type__title',
        'roles__title',
    )

    def get_roles_as_string(self, obj):
        """Return all the roles titles as a single string"""
        return ', '.join(
            (role.title for role in obj.roles.all())
        )
    get_roles_as_string.short_description = "roles"

@admin.register(MembershipFee)
class MembershipFeeAdmin(admin.ModelAdmin):
    "How the MembershipFee model can be manage from admin"


@admin.register(InternalRegulation)
class InternalRegulationAdmin(admin.ModelAdmin):
    "How the InternalRegulation model can be manage from admin"
    list_display = ('version', 'is_applied')

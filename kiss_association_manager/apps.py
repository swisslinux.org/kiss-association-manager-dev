from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class KissAssociationManagerConfig(AppConfig):
    name = 'kiss_association_manager'
    verbose_name = _('Association manager')

from django.test import TestCase
from datetime import date

from kiss_association_manager.models import (
    YEAR_CODE,
    MONTH_CODE,
    Member,
    MemberType,
    Address,
    MembershipFee,
)


class MembershipFeeTestCase(TestCase):
    def setUp(self):
        member_type_annual = MemberType.objects.create(
            title='annual member',
            membership_fee_amount=100.0,
            membership_fee_frequencies=YEAR_CODE,
        )
        member_type_month = MemberType.objects.create(
            title='month member',
            membership_fee_amount=15.0,
            membership_fee_frequencies=MONTH_CODE,
        )
        address_test = Address.objects.create(
            street=' ',
            postal_code=' ',
            city=' ',
            state=' ',
            country=' ',
        )
        Member.objects.create(
            forname='Bob',
            name=' ',
            address=address_test,
            email='bob@exemple.com',
            state=Member.MEMBER_CODE,
            member_type=member_type_annual,
        )
        Member.objects.create(
            forname='Alice',
            name=' ',
            address=address_test,
            email='alice@exemple.com',
            state=Member.MEMBER_CODE,
            member_type=member_type_annual,
        )
        Member.objects.create(
            forname='Joe',
            name=' ',
            address=address_test,
            email='alice@exemple.com',
            state=Member.MEMBER_CODE,
            member_type=member_type_month,
        )

    def test_membership_fee_generate_missing_annuals(self):
        MembershipFee.generate_missing_annuals(date.today().year)
        self.assertEqual(MembershipFee.objects.all().count(), 2)

        self.assertTrue(
            MembershipFee.objects.filter(
                member__forname='Bob',
                amount=100.0,
                period_length=YEAR_CODE,
                from_date__year=date.today().year,
                to_date__year=date.today().year,
                payment_status=MembershipFee.TO_PAY_CODE,
            ).exists()
        )
        self.assertTrue(
            MembershipFee.objects.filter(
                member__forname='Alice',
                amount=100.0,
                period_length=YEAR_CODE,
                from_date__year=date.today().year,
                to_date__year=date.today().year,
                payment_status=MembershipFee.TO_PAY_CODE,
            ).exists()
        )

        MembershipFee.generate_missing_annuals(date.today().year)
        self.assertEqual(MembershipFee.objects.all().count(), 2)
        
        MembershipFee.generate_missing_annuals(date.today().year-1)
        self.assertEqual(MembershipFee.objects.all().count(), 4)

    def test_membership_fee_is_sent(self):
        member = Member.objects.first()
        membership_fee = MembershipFee.objects.create(
            title='',
            member=member,
            amount=48.0,
            from_date=date.today(),
            to_date=date.today(),
            period_length=MONTH_CODE,
        )

        self.assertFalse(membership_fee.is_sent())
        membership_fee.sending_date = date.today()
        self.assertTrue(membership_fee.is_sent())
